import threading
import os, weakref, sys

__author__ = "Tomasz Hasinski <tomasz.hasinski@prohas.pl>"
__status__ = "production"
# The following module attributes are no longer updated.
__version__ = "0.1"
__date__ = "15 December 2019"

__all__ = ['setSettings', 'getSettings', 'get_settings_class', 'set_settings_class', 'Settings']


def set_settings_class(klass):
    """
    Set the class to be used when instantiating a logger. The class should
    define __init__() such that only a name argument is required, and the
    __init__() should call Logger.__init__()
    """
    if klass != Settings:
        if not issubclass(klass, Settings):
            raise TypeError("logger not derived from logging.Logger: "
                            + klass.__name__)
    global _settingsClass
    _settingsClass = klass


def get_settings_class():
    """
    Return the class to be used when instantiating a logger.
    """
    return _settingsClass


# ---------------------------------------------------------------------------
#   Thread-related stuff
# ---------------------------------------------------------------------------

#
# _lock is used to serialize access to shared data structures in this module.
# This needs to be an RLock because fileConfig() creates and configures
# Handlers, and so might arbitrary user threads. Since Handler code updates the
# shared dictionary _handlers, it needs to acquire the lock. But if configuring,
# the lock would already have been acquired - so we need an RLock.
# The same argument applies to Loggers and Manager.loggerDict.
#

_lock = threading.RLock()


def _acquire_lock():
    """
    Acquire the module-level lock for serializing access to shared data.

    This should be released with _release_lock().
    """
    if _lock:
        _lock.acquire()


def _release_lock():
    """
    Release the module-level lock acquired by calling _acquire_lock().
    """
    if _lock:
        _lock.release()


class SettingsManager(object):
    """
    There is [under normal circumstances] just one Manager instance, which
    holds the hierarchy of loggers.
    """

    def __init__(self, rootnode):
        """
        Initialize the manager with the root node of the logger hierarchy.
        """
        self.settingsDict = {}
        self.settingsClass = None
        self.root = rootnode

    def setSettings(self, name, value, read_only=False):
        _acquire_lock()
        try:
            if not isinstance(name, str):
                raise TypeError('A setting name must be a string')
            if name in self.settingsDict:
                rv = self.settingsDict[name]
                if isinstance(rv, PlaceHolder):
                    ph = rv
                    rv = (self.settingsClass or _settingsClass)(name, value, read_only)
                    rv.manager = self
                    self.settingsDict[name] = rv
                    self._fixupChildren(ph, rv)
                    self._fixupParents(rv)
                else:
                    if rv.read_only:
                        raise IOError("Setting {} is read only!".format(name))
                    else:
                        rv.value = value
                        rv.read_only = read_only
            else:
                rv = Settings(name, value, read_only)
                self.settingsDict[name] = rv
                self._fixupParents(rv)
        finally:
            _release_lock()

    def getSettings(self, name):
        """
        Get a configuration with the specified name (channel name), creating it
        if it doesn't yet exist.
        """
        rv = None
        if not isinstance(name, str):
            raise TypeError('A setting name must be a string')
        _acquire_lock()
        try:
            if name in self.settingsDict:
                rv = self.settingsDict[name]
            else:
                raise ValueError('No setting name: {}'.format(name))
        finally:
            _release_lock()
        return rv

    def _fixupParents(self, alogger):
        """
        Ensure that there are either loggers or placeholders all the way
        from the specified logger to the root of the logger hierarchy.
        """
        name = alogger.name
        i = name.rfind(".")
        rv = None
        while (i > 0) and not rv:
            substr = name[:i]
            if substr not in self.settingsDict:
                self.settingsDict[substr] = PlaceHolder(alogger)
            else:
                obj = self.settingsDict[substr]
                if isinstance(obj, Settings):
                    rv = obj
                else:
                    assert isinstance(obj, PlaceHolder)
                    obj.append(alogger)
            i = name.rfind(".", 0, i - 1)
        if not rv:
            rv = self.root
        alogger.parent = rv

    def _fixupChildren(self, ph, alogger):
        """
        Ensure that children of the placeholder ph are connected to the
        specified logger.
        """
        name = alogger.name
        namelen = len(name)
        for c in ph.loggerMap.keys():
            # The if means ... if not c.parent.name.startswith(nm)
            if c.parent.name[:namelen] != name:
                alogger.parent = c.parent
                c.parent = alogger


class PlaceHolder(object):
    """
    PlaceHolder instances are used in the Manager logger hierarchy to take
    the place of nodes for which no loggers have been defined. This class is
    intended for internal use only and not as part of the public API.
    """

    def __init__(self, alogger):
        """
        Initialize with the specified logger being a child of this placeholder.
        """
        self.loggerMap = {alogger: None}
        self.value = None

    def append(self, alogger):
        """
        Add the specified logger as a child of this placeholder.
        """
        if alogger not in self.loggerMap:
            self.loggerMap[alogger] = None


class Settings(object):

    def __init__(self, name, value, read_only=False):
        self.manager = _settings_manager
        self.name = name
        self.value = value
        self.parent = None
        self.propagate = True
        self.read_only = read_only

    def __reduce__(self):
        # In general, only the root logger will not be accessible via its name.
        # However, the root logger's class has its own __reduce__ method.
        if getSettings(self.name) is not self:
            import pickle
            raise pickle.PicklingError('logger cannot be pickled')
        return getSettings, (self.name,)

    def __repr__(self):
        return "<Settings  {}: {} object>".format(self.name, self.value)


class RootSettings(Settings):
    """
    A root logger is not that different to any other logger, except that
    it must have a logging level and there is only one instance of it in
    the hierarchy.
    """

    def __init__(self):
        """
        Initialize the settings with the name "root".
        """
        self.name = "root"
        self.value = None
        self.parent = None
        self.propagate = False
        self.manager = None

    def __repr__(self):
        return '<Root Settings object>'

    def __reduce__(self):
        return getSettings, ()


def getSettings(name=None):
    if name:
        return _settings_manager.getSettings(name).value
    else:
        return root_node


def setSettings(name, value, read_only=False):
    if not isinstance(name, str):
        raise TypeError('A setting name must be a string')
    return _settings_manager.setSettings(name, value, read_only)


_settingsClass = Settings
root_node = RootSettings()
_settings_manager = SettingsManager(root_node)
root_node.manager = _settings_manager
