import settings
import pytest


def test_set_settings():
    name = 'test1'
    value = 'test2'
    settings.setSettings('test1', 'test2')
    assert name in settings._settings_manager.settingsDict.keys()
    assert isinstance(settings._settings_manager.settingsDict[name], settings.Settings)
    assert value == settings._settings_manager.settingsDict[name].value
    assert name == settings._settings_manager.settingsDict[name].name
    assert isinstance(settings._settings_manager.settingsDict[name].parent, settings.RootSettings)
    assert True == settings._settings_manager.settingsDict[name].propagate
    assert isinstance(settings._settings_manager.settingsDict[name].manager,
                      settings.SettingsManager)
    assert settings._settings_manager.settingsDict[name].read_only == False


def test_set_settings_repr():
    name = 'test1'
    value = 'test2'
    settings.setSettings('test1', 'test2')
    p = str(settings._settings_manager.settingsDict[name])
    assert isinstance(p, str)
    assert "<" in p
    assert ">" in p
    assert name in p
    assert value in p


def test_get_settings():
    name = 'test1'
    value = 'test2 wp4fi wpinwf wf'
    settings.setSettings(name, value)
    settings.setSettings('test4', 'test44')
    v2 = settings.getSettings(name)
    assert value == v2
    v4 = settings.getSettings('test4')
    assert 'test44' == v4


def test_set_settings_structure():
    parent = 'parenttest'
    parent2 = 'parent3'
    name = '{}.{}.test1'.format(parent, parent2)
    value = 'test2 wp4fi wpinwf wf'
    settings.setSettings(name, value)

    assert value == settings.getSettings(name)
    assert parent in settings._settings_manager.settingsDict.keys()
    assert None == settings.getSettings(parent)
    assert isinstance(settings._settings_manager.settingsDict[parent], settings.PlaceHolder)

    name2 = "{}.{}".format(parent, parent2)
    assert name2 in settings._settings_manager.settingsDict.keys()
    assert None == settings.getSettings(name2)
    assert isinstance(settings._settings_manager.settingsDict[name2], settings.PlaceHolder)


def test_set_settings_errors():
    name = 'test231'
    value1 = 'test2 wp4fi wpinwf wf'
    with pytest.raises(ValueError) as e:
        settings.getSettings(name)
    assert name in str(e.value)


def test_set_settings_read_only():
    name = 'test7'
    value1 = 'test2 wp4fi wpinwf wf'
    settings.setSettings(name, value1, True)
    value2 = settings.getSettings(name)
    assert value1 == value2
    value3 = "wfrfbwe"
    with pytest.raises(IOError) as e:
        settings.setSettings(name, value3)
    assert name in str(e.value)
    assert settings._settings_manager.settingsDict[name].read_only == True


def test_set_settings_errors_read_only():
    name = 'test187'
    value1 = 'test2 wp4fi wpinwf wf'
    with pytest.raises(ValueError) as e:
        settings.getSettings(name)
    assert name in str(e.value)
